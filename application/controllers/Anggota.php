<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {
/**
	KONTOL
**/
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('Anggota_model');
	}

	 public function index()
	{
		if (!isset($_SESSION['TelahLogin'])) {
			redirect('main/login');
		}else{

			/** Start Setting User */
			$this->load->model('Setting_model');
			$data['user'] = $this->Setting_model->lihat_user()->result();

			$this->load->view('setting', $data);
			
			/** End Setting User */
			
			$data['anggota'] = $this->Anggota_model->lihat_anggota()->result();
			$this->load->view('template');

			$this->load->view('anggota/index.php',$data);
		}
	}

	public function tambah()
	{
		if (!isset($_SESSION['TelahLogin'])) {
			redirect('main/login');
		}else{			

			/** Start Setting User */
			$this->load->model('Setting_model');
			$data['user'] = $this->Setting_model->lihat_user()->result();

			$this->load->view('setting', $data);
			
			/** End Setting User */
		$data['tanggal_daftar']	=	date('Y-m-d');
		$this->load->view('anggota/tambah',$data);
		$this->load->view('template');

		}
	}

	public function tambah_aksi()
	{
		$nis 			= $this->input->post('nis');
		$nama 			= $this->input->post('nama');
		$jenis_kelamin 	= $this->input->post('jenis_kelamin');
		$tanggal_lahir 	= $this->input->post('tanggal_lahir');
		$tanggal_daftar = $this->input->post('tanggal_daftar');
		$kelas 			= $this->input->post('kelas');

		$data = array(
			'nis' 				=> $nis,
			'nama'				=> $nama,
			'jenis_kelamin'		=> $jenis_kelamin,
			'tanggal_lahir'		=> $tanggal_lahir,
			'tanggal_daftar'	=> $tanggal_daftar,
			'kelas' 			=> $kelas
			);
		$this->Anggota_model->tambah_data($data,'anggota');
		redirect('anggota/index');
	}

	function hapus($nis){
		$where = array('nis' => $nis);
		$this->Anggota_model->hapus_data($where,'anggota');
		redirect('anggota/index');
	}

	function edit_aksi()
	{
		$nis 			= $this->input->post('nis');
		$nama 			= $this->input->post('nama');
		$jenis_kelamin 	= $this->input->post('jenis_kelamin');
		$tanggal_lahir 	= $this->input->post('tanggal_lahir');
		$jurusan 		= $this->input->post('jurusan');
		$kelas 			= $this->input->post('kelas');

		$data = array(
			'nis' 			=> $nis,
			'nama'			=> $nama,
			'jenis_kelamin'	=> $jenis_kelamin,
			'tanggal_lahir'	=> $tanggal_lahir,
			'jurusan'		=> $jurusan,
			'kelas' 		=> $kelas
			);

		$where = array(
			'nis' => $nis
			);

		$this->Anggota_model->update_data($where,$data,'anggota');
		redirect('anggota/index');
		}

	public function edit($nis)
	{
		if (!isset($_SESSION['TelahLogin'])) {
			redirect('main/login');
		}else{

			/** Start Setting User */
			$this->load->model('Setting_model');
			$data['user'] = $this->Setting_model->lihat_user()->result();

			$this->load->view('setting', $data);
			
			/** End Setting User */

		$where = array('nis' => $nis);
		$data['anggota'] = $this->Anggota_model->edit_data($where,'anggota')->result();
		
		$this->load->view('anggota/edit', $data);
		$this->load->view('template');
		}
	}
}
/* End of file Anggota.php */
/* Location: ./application/controllers/Anggota.php */