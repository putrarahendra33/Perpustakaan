<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('Buku_model');
	}

	 public function index()
	{
		if (!isset($_SESSION['TelahLogin'])) {
			redirect('main/login');
		}else{

			/** Start Setting User */
			$this->load->model('Setting_model');
			$data['user'] = $this->Setting_model->lihat_user()->result();

			$this->load->view('setting', $data);
			
			/** End Setting User */

			$data['buku'] = $this->Buku_model->lihat_buku()->result();
			$this->load->view('template');

			$this->load->view('buku/index.php',$data);
		}
	}

	public function tambah()
	{
		if (!isset($_SESSION['TelahLogin'])) {
			redirect('main/login');
		}else{

			/** Start Setting User */
			$this->load->model('Setting_model');
			$data['user'] = $this->Setting_model->lihat_user()->result();

			$this->load->view('setting', $data);
			
			/** End Setting User */

		$this->load->view('buku/tambah');
		$this->load->view('template');

		}
	}

	public function tambah_aksi()
	{
		$kode_buku 		= $this->input->post('kode_buku');
		$judul 		 	= $this->input->post('judul');
		$pengarang 		= $this->input->post('pengarang');
		$kategori_buku 	= $this->input->post('kategori_buku');

		$data = array(
			'kode_buku' 	=> $kode_buku,
			'judul'			=> $judul,
			'pengarang'		=> $pengarang,
			'kategori_buku'	=> $kategori_buku,
			);
		$this->Buku_model->tambah_data($data,'buku');
		redirect('buku/index');
	}

	function hapus($kode_buku){
		$where = array('kode_buku' => $kode_buku);
		$this->Buku_model->hapus_data($where,'buku');
		redirect('buku/index');
	}

	function edit_aksi()
	{
		$kode_buku 		= $this->input->post('kode_buku');
		$judul 			= $this->input->post('judul');
		$pengarang 		= $this->input->post('pengarang');
		$kategori_buku 	= $this->input->post('kategori_buku');

		$data = array(
			'kode_buku' 	=> $kode_buku,
			'judul'			=> $judul,
			'pengarang'		=> $pengarang,
			'kategori_buku'	=> $kategori_buku,
			);

		$where = array(
			'kode_buku' => $kode_buku
			);

		$this->Buku_model->update_data($where,$data,'buku');
		redirect('buku/index');
		}

	public function edit($kode_buku)
	{
		if (!isset($_SESSION['TelahLogin'])) {
			redirect('main/login');
		}else{

			/** Start Setting User */
			$this->load->model('Setting_model');
			$data['user'] = $this->Setting_model->lihat_user()->result();

			$this->load->view('setting', $data);
			
			/** End Setting User */

		$where = array('kode_buku' => $kode_buku);
		$data['buku'] = $this->Buku_model->edit_data($where,'buku')->result();
		
		$this->load->view('buku/edit', $data);
		$this->load->view('template');
		}
	}

}

/* End of file buku.php */
/* Location: ./application/controllers/buku.php */