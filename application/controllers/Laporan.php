<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
/*
	Kontol
*/
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form'));
		$this->load->library('session');
		$this->load->model('Laporan_Model');
	}

	public function index()
	{
		if (!isset($_SESSION['TelahLogin'])) {
			redirect('main/login');
		}else{

			/** Start Setting User */
			$this->load->model('Setting_model');
			$data['user'] = $this->Setting_model->lihat_user()->result();

			$this->load->view('setting', $data);
			
			/** End Setting User */

			/** Start MAIN Function */
			$data['anggota'] 		= $this->Laporan_Model->dapat_anggota();
			$data['buku'] 			= $this->Laporan_Model->dapat_buku();
			$data['peminjaman']		= $this->Laporan_Model->dapat_peminjaman();
			$data['pengembalian'] 	= $this->Laporan_Model->dapat_pengembalian();

			$this->load->view('beranda/laporan',$data);
			$this->load->view('template');
		}
	}

	public function anggota()
	{
		if (!isset($_SESSION['TelahLogin'])){
			redirect('main/login');
		}else{

			/** Start Setting User */
			$this->load->model('Setting_model');
			$data['user'] = $this->Setting_model->lihat_user()->result();

			$this->load->view('setting', $data);
			
			/** End Setting User */

			/** Start MAIN Function */
			$data['anggota'] = $this->Laporan_Model->lihatAnggota()->result();
			$this->load->view('laporan/anggota',$data);
			$this->load->view('template');
		}
	}

	public function buku()
	{
		if (!isset($_SESSION['TelahLogin'])){
			redirect('main/login');
		}else{

			/** Start Setting User */
			$this->load->model('Setting_model');
			$data['user'] = $this->Setting_model->lihat_user()->result();

			$this->load->view('setting', $data);
			
			/** End Setting User */

			/** Start MAIN Function */
			$data['buku'] = $this->Laporan_Model->lihatBuku()->result();
			$this->load->view('laporan/buku',$data);
			$this->load->view('template');
		}
	}

	public function peminjaman()
	{
		if (!isset($_SESSION['TelahLogin'])){
			redirect('main/login');
		}else{

			/** Start Setting User */
			$this->load->model('Setting_model');
			$data['user'] = $this->Setting_model->lihat_user()->result();

			$this->load->view('setting', $data);
			
			/** End Setting User */

			/** Start MAIN Function */
			$this->load->view('laporan/peminjaman',$data);
			$this->load->view('template');
		}
	}
	function cari_peminjaman()
	{
		$tanggal1			=	$this->input->post('tanggal1');
		$tanggal2			=	$this->input->post('tanggal2');
		$data['laporan']	=	$this->Laporan_Model->detailPeminjaman($tanggal1,$tanggal2)->result();
		$this->load->view('laporan/cari_pinjaman',$data);
	}

	public function pengembalian()
	{
		if (!isset($_SESSION['TelahLogin'])){
			redirect('main/login');
		}else{

			/** Start Setting User */
			$this->load->model('Setting_model');
			$data['user'] = $this->Setting_model->lihat_user()->result();

			$this->load->view('setting', $data);
			
			/** End Setting User */

			/** Start MAIN Function */
			$this->load->view('laporan/pengembalian',$data);
			$this->load->view('template');
		}
	function cari_pengembalian ()
	{
		$tanggal1			=	$this->input->post('tanggal1');
		$tanggal2			=	$this->input->post('tanggal2');
		$data['laporan']	=	$this->Laporan_Model->detailPengembalian($tanggal1,$tanggal2)->result();
		$this->load->view('laporan/cari_pengembalian',$data);
		}

	}
}

/* End of file laporan.php */
/* Location: ./application/controllers/laporan.php */