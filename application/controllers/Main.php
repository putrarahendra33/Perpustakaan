<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('Main_Model');
	}

	public function index()
	{
		if(isset($_SESSION['TelahLogin'])){
			redirect('main/beranda');
		}
		$this->load->view('login/index');
	}

	public function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => $password
			);

		$proses = $this->Main_Model->proses_login("user", $where)->num_rows();
		if($proses > 0){
			$data_session = array(
				'TelahLogin' => $username,
				'status' => "login",
				);
			$this->session->set_userdata($data_session);
			redirect('main/beranda');
		}else{
			echo '<script>alert("Login Gagal");</script>';
		}
	}
	
	function logout(){
		$this->session->sess_destroy();
		$this->load->view('login/index');

	}

	/** function edit_user_aksi()
	{
		$this->load->model('Setting_model');

		$id 		= $this->input->post('id');
		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$nama_asli 	= $this->input->post('nama_asli');

		$data = array(
			'id' 		=> $id,
			'username'	=> $username,
			'password'	=> $password,
			'nama_asli'	=> $nama_asli
			);

		$where = array(
			'id' => $id
			);

		$this->Setting_model->update_data($where,$data,'user');
		} */

	public function beranda()
	{
		if (!isset($_SESSION['TelahLogin'])) {
			redirect('main/login');
		}else{
			/** Start Setting User */
			$this->load->model('Setting_model');
			$data['user'] = $this->Setting_model->lihat_user()->result();

			$this->load->view('setting', $data);
			
			/** End Setting User */
			$data['anggota'] = $this->Main_Model->get_anggota();
			$data['buku'] = $this->Main_Model->get_buku();

		$this->load->model('Main_Model');
		$this->load->view('beranda/index', $data);
		$this->load->view('template');
		}
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */