<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peminjaman extends CI_Controller {
/*
	Kontol
*/
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form'));
		$this->load->library('session');
		$this->load->model('Peminjaman_model');
	}

	public function index()
	{
		if (!isset($_SESSION['TelahLogin'])) {
			redirect('main/login');
		}else{

			/** Start Setting User */
			$this->load->model('Setting_model');
			$data['user'] = $this->Setting_model->lihat_user()->result();

			$this->load->view('setting', $data);
			/** End Setting User */

			/** Start MAIN Function **/
			$data['tanggalpinjam']	=	date('Y-m-d');
        	$data['tanggalkembali'] = 	strtotime('+7 day',strtotime($data['tanggalpinjam']));
        	$data['noauto']			=	bin2hex(openssl_random_pseudo_bytes(3));
        	$data['anggota']		=	$this->Peminjaman_model->getAnggota()->result();
        	$data['tanggalkembali'] = 	date('Y-m-d', $data['tanggalkembali']);
			$this->load->view('Peminjaman/index',$data);

			$this->load->view('template'); 
		}

	}

	public function tampil()
	{
		$data['tmp']		= $this->Peminjaman_model->tampilTmp()->result();
		$data['jumlahTmp']	= $this->Peminjaman_model->jumlahTmp(); 
		$this->load->view('peminjaman/tampil', $data);
	}

	public function cariAnggota(){
        $nis=$this->input->post('nis');
        $anggota=$this->Peminjaman_model->cariAnggota($nis);
        if($anggota->num_rows()>0){
            $anggota=$anggota->row_array();
            echo $anggota['nama'];
        }
    }
    
    public function cariBuku(){
        $kode=$this->input->post('kode_buku');
        $buku=$this->Peminjaman_model->cariBuku($kode);
        if($buku->num_rows()>0){
            $buku=$buku->row_array();
            echo $buku['judul_buku']."|".$buku['pengarang']."|".$buku['kategori_buku'];
        }
    }

    public function tambah(){
        $kode=$this->input->post('kode_buku');
        $cek=$this->Peminjaman_model->cekTmp($kode);
        if($cek->num_rows()<1){
            $info=array(
                'kode_buku'			=>$this->input->post('kode'),
                'judul_buku'		=>$this->input->post('judul_buku'),
                'pengarang'			=>$this->input->post('pengarang'),
                'kategori'			=>$this->input->post('kategori_buku')
            );
            $this->Peminjaman_model->simpanTmp($info);
        }
    }
    
    function hapus(){
        $kode=$this->input->post('kode_buku');
        $this->Peminjaman_model->hapusTmp($kode);
    	}
    
    
    function pencarianbuku(){
        $cari=$this->input->post('caribuku');
        $data['buku']=$this->Peminjaman_model->pencarianbuku($cari)->result();
        $this->load->view('peminjaman/cari_buku',$data);
    }
}

/* End of file Peminjaman.php */
/* Location: ./application/controllers/Peminjaman.php */