<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_Model extends CI_Model {

	public function dapat_anggota()
	{
		$this->db->select('*');
		$this->db->from('anggota');
		$this->db->order_by('nis', 'DESC');

		$lihat = $this->db->get();

		return $lihat->num_rows();
	}

	public function dapat_buku()
	{
		$this->db->select('*');
		$this->db->from('buku');
		$this->db->order_by('kode_buku', 'DESC');

		$lihat = $this->db->get();

		return $lihat->num_rows();
	}

	public function dapat_peminjaman()
	{
		$this->db->select('*');
		$this->db->from('peminjaman');
		$this->db->order_by('id_peminjaman', 'DESC');

		$lihat = $this->db->get();

		return $lihat->num_rows();
	}

	public function dapat_pengembalian()
	{
		$this->db->select('*');
		$this->db->from('pengembalian');
		$this->db->order_by('tgl_pengembalian', 'DESC');

		$lihat = $this->db->get();

		return $lihat->num_rows();
	}

	public function lihatAnggota()
	{
		return $this->db->get("anggota");
	}

	public function lihatBuku()
	{
		return $this->db->get("buku");
	}
	

}

/* End of file laporan_Model.php */
/* Location: ./application/models/laporan_Model.php */