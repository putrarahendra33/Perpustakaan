<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_Model extends CI_Model {

	public function proses_login($table,$where)
	{
		return $this->db->get_where($table,$where);
	}
	
	public function get_anggota()
	{
		$this->db->select('*');
		$this->db->from('anggota');
		$this->db->order_by('nis', 'DESC');

		$lihat = $this->db->get();

		return $lihat->num_rows();
	}

	public function get_buku()
	{
		$this->db->select('*');
		$this->db->from('buku');
		$this->db->order_by('kode_buku', 'DESC');

		$lihat = $this->db->get();

		return $lihat->num_rows();
	}

}

/* End of file main_Models.php */
/* Location: ./application/models/main_Models.php */