<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peminjaman_model extends CI_Model {

	public function getAnggota()
	{
		return $this->db->get('anggota');
	}

    public function cariAnggota($nis)
	{
		$this->db->where('nis', $nis);
		return $this->db->get('anggota');
	}

	public function cariBuku($kode)
	{
        $this->db->where("kode_buku",$kode);
        return $this->db->get("buku");
    }
    
    public function simpanTmp($info)
    {
        $this->db->insert("temporary",$info);
    }
    
    public function tampilTmp()
    {
        return $this->db->get("temporary");
    }
    
    public function cekTmp($kode){
        $this->db->where("kode_buku",$kode);
        return $this->db->get("temporary");
    }
    
    public function jumlahTmp()
    {
        return $this->db->count_all("temporary");
    }
    
    public function hapusTmp($kode)
    {
        $this->db->where("kode_buku",$kode);
        $this->db->delete("temporary");
    }
    
    public function simpan($info)
    {
        $this->db->insert("transaksi",$info);
    }
    
    public function pencarianbuku($cari)
    {
        $this->db->like("judul_buku",$cari);
        return $this->db->get("buku");
    }
}

/* End of file Peminjaman_model.php */
/* Location: ./application/models/Peminjaman_model.php */