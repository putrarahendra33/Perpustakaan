<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model {

	public function lihat_user()
	{
		return $this->db->get('user');
	}
	
	public function edit_data($where,$table)
	{
		return $this->db->get_where($table,$where);
	}

	public function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}


}

/* End of file Setting_model.php */
/* Location: ./application/models/Setting_model.php */