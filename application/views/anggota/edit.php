<!DOCTYPE html>
<html>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    Data Anggota
                    <small>Kon <a href="http://localhost/ta1/sistem_perpustakaan/main/anggota" target="_blank">tol</a></small>
                </h2>
            </div>
            <?php foreach($anggota as $a){ ?>
            <form action="<?php echo base_url(). 'anggota/edit_aksi'; ?>" method="post">
            <div class="panel panel-default">
  				<div class="panel-heading"><b>Edit Data Anggota</b></div>
  					<div class="panel-body">
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">NIS:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="nis" maxlength="6" minlength="3" required class="form-control" placeholder="Masukan NIS" value="<?php echo $a->nis ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nama">Nama:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="nama" class="form-control" placeholder="Masukan Nama Lengkap" value="<?php echo $a->nama ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="jk">Jenis Kelamin:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <select name="jenis_kelamin" class="control show-tick" value="<?php echo $a->jenis_kelamin ?>">
                                                <option value="LK">Laki-Laki</option>
                                                <option value="PR">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="ttl">Tanggal Lahir:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="date" name="tanggal_lahir" class="form-control date" value="<?php echo $a->tanggal_lahir ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nama">Tanggal Daftar:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="tanggal_daftar" class="form-control" value="<?php echo $a->tanggal_daftar; ?>" readonly="readonly">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="jurusan">:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <select name="jurusan" class="control show-tick <?php /** echo $a->jurusan */?>">
                                                <option value="AK">Akuntansi</option>
                                                <option value="APH">Akomodasi Perhotelan</option>
                                                <option value="APK">Adminitrasi Perkantoran</option>
                                                <option value="DKV">Desain Komunikasi Visual</option>
                                                <option value="MM">Multimedia</option>
                                                <option value="PMSR">Pemasaran</option>
                                                <option value="RPL">Rekayasa Perangkat Lunak</option>
                                                <option value="TKJ">Teknik Komputer Jaringan</option>
                                                <option value="TPPPP">Boardcasting</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> -->
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="kelas">Kelas:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select name="kelas" class="control show-tick <?php echo $a->kelas ?>">
                                                <option value="XII RPL 1">XII RPL 1</option>
                                                <option value="XII RPL 2">XII RPL 2</option>
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">SIMPAN</button>
                                    </div>
                                </div>
                            </tr>
                        </table>
                    </form>
                    <?php } ?>


