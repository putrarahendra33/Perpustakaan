
<!DOCTYPE html>
<html>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>MASTER ADMINISTRATOR</h2>
            </div>

            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="anggota()">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">account_box</i>
                        </div>
                        <div class="content">
                            <div class="text">Anggota</div>
                            <div class="number count-to" data-from="0" data-to="<?php echo $anggota ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="buku()">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">book</i>
                        </div>
                        <div class="content">
                            <div class="text">Buku</div>
                            <div class="number count-to"  -from="0" data-to="<?php echo $buku ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="peminjaman()">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">                           
                            <i class="material-icons">file_download</i>
                        </div>
                        <div class="content">
                            <div class="text">Peminjaman</div>
                            <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="pengembalian()">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">done_all</i>
                        </div>
                        <div class="content">
                            <div class="text">Pengembalian</div>
                            <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div><div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="laporan()">
                    <div class="info-box bg-red hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">receipt</i>
                        </div>
                        <div class="content">
                            <div class="text">Laporan</div>
                            <div class="number count-to" data-from="0" data-to="4" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->

</html>

<script type="text/javascript">
    function anggota()
    {
        window.location = "http://localhost/ta1/sistem_perpustakaan/anggota";
    }

    function buku()
    {
        window.location = "http://localhost/ta1/sistem_perpustakaan/buku";
    }

    function peminjaman()
    {
        window.location = "http://localhost/ta1/sistem_perpustakaan/peminjaman";
    }

    function pngembalian()
    {
        window.location = "http://localhost/ta1/sistem_perpustakaan/pengembalian";
    }

    function laporan()
    {
        window.location = "http://localhost/ta1/sistem_perpustakaan/laporan/";
    }
</script>