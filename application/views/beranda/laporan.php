
<!DOCTYPE html>
<html>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>MASTER LAPORAN</h2>
            </div>

            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="anggota()">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">account_box</i>
                        </div>
                        <div class="content">
                            <div class="text">Data Anggota</div>
                            <div class="number count-to" data-from="0" data-to="<?php echo $anggota ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="buku()">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">book</i>
                        </div>
                        <div class="content">
                            <div class="text">Data Buku</div>
                            <div class="number count-to"  -from="0" data-to="<?php echo $buku ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="peminjaman()">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">                           
                            <i class="material-icons">file_download</i>
                        </div>
                        <div class="content">
                            <div class="text">Data Pinjaman</div>
                            <div class="number count-to" data-from="0" data-to="<?php echo $peminjaman ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="pengembalian()">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">done_all</i>
                        </div>
                        <div class="content">
                            <div class="text">Data Pengembalian</div>
                            <div class="number count-to" data-from="0" data-to="<?php echo $pengembalian
                             ?>" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->

</html>

<script type="text/javascript">
    function anggota()
    {
        window.location = "http://localhost/ta1/sistem_perpustakaan/laporan/anggota";
    }

    function buku()
    {
        window.location = "http://localhost/ta1/sistem_perpustakaan/laporan/buku";
    }

    function peminjaman()
    {
        window.location = "http://localhost/ta1/sistem_perpustakaan/laporan/peminjaman";
    }

    function pngembalian()
    {
        window.location = "http://localhost/ta1/sistem_perpustakaan/laporan/pengembalian";
    }
</script>