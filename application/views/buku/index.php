<!DOCTYPE html>
<html>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    Data Buku
                    <small>Taken from <a href="http://localhost/ta1/sistem_perpustakaan/anggota" target="_blank">localhost</a></small>
                </h2>
            </div>
            <div class="panel panel-default">
  				<div class="panel-heading"><b>Data Buku</b></div>
  					<div class="panel-body">
  						<a href="<?php echo base_url(). 'buku/tambah'; ?>" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
       						<table class="table table-striped">
						        <thead>
						         <tr>
						         <th>No</th>
						         <th>Kode Buku</th>
						         <th>Judul Buku</th>
						         <th>Pengarang</th>
						         <th>Kategori Buku</th>
						         <th>Aksi</th>
						         <th></th>
						         </tr>
						        </thead>
						        <tbody>
						        <?php 
						        $no=1;
						        	if (count($buku)==NULL) {
						        		echo '<td colspan="6">Data Tidak Ada !!!</td>';
						        	}
						        	else {


						        foreach ($buku as $buku) { ?>
						        <td><?php echo $no++ ?></td>
						        <td><?php echo $buku->kode_buku ?></td>
						        <td><?php echo $buku->judul ?></td>
						        <td><?php echo $buku->pengarang ?></td>
						        <td><?php echo $buku->kategori_buku ?></td>
						        <td><a href="<?php echo base_url().'buku/edit/'.$buku->kode_buku ?>"><i class="large material-icons">mode_edit</i></a>
						        <a href="<?php echo base_url().'buku/hapus/'.$buku->kode_buku ?>"><i class="large material-icons">delete</i></a></td>
						        <tr>
						        <?php } } ?>
						   </tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</html>
