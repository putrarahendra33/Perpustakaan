<!DOCTYPE html>
<html>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    Data Buku
                    <small>Kon <a href="http://localhost/ta1/sistem_perpustakaan/buku" target="_blank">tol</a></small>
                </h2>
            </div>
            <form action="<?php echo base_url(). 'buku/tambah_aksi'; ?>" method="post">
            <div class="panel panel-default">
  				<div class="panel-heading"><b>Data Buku</b></div>
  					<div class="panel-body">
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nis">Kode Buku:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="kode_buku" maxlength="6" minlength="3" required class="form-control" placeholder="Masukan Kode Buku">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="nama">Judul Buku:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="judul_buku" class="form-control" placeholder="Masukan Judul Buku">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="kelas">Pengarang:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="pengarang" class="form-control" placeholder="Masukan Nama Pengarang">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="jk">Kategori Buku</label>
                                    </div>
                                    <div class="col-lg-4 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <select name="kategori_buku" class="control show-tick">
                                                <option value="Novel">Novel</option>
                                                <option value="Majalah">Majalah</option>
                                                <option value="Ensiklopedia">Ensiklopedia</option>
                                                <option value="Paket">Paket</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">TAMBAH</button>
                                    </div>
                                </div>
                            </tr>
                        </table>
                    </form>


