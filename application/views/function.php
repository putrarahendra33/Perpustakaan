<?php

function ConvertWaktu($date)
{
	$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
 
	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);

	$jam = substr($date, 11, 2);
	$menit = substr($date, 14, 2);
 
	$result = $tgl." ".$BulanIndo[(int)$bulan-1]." ".$tahun." Pukul ".$jam." : ".$menit." WIB";		
	return($result);
}

function ConvertTanggal($date)
{
	$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
 
	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);
 
	$result = $tgl." ".$BulanIndo[(int)$bulan-1]." ".$tahun;		
	return($result);
}

?>