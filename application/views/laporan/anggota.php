<!DOCTYPE html>
<html>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    Data Anggota
                    <small>Taken from <a href="http://localhost/ta1/sistem_perpustakaan/main/anggota" target="_blank">localhost</a></small>
                </h2>
            </div>
            <div class="panel panel-default">
  				<div class="panel-heading"><b>Data Anggota</b></div>
  					<div class="panel-body">
       						<table class="table table-striped dataTable js-exportable">
						        <thead>
						         <tr>
						         <th>No</th>
						         <th>NIS</th>
						         <th>Nama</th>
						         <th>JK</th>
						         <th>Tanggal Lahir</th>
						         <th>Tanggal Daftar</th>
						         <th>Kelas</th>
						         <th></th>
						         </tr>
						        </thead>
						        <tbody>
						        <?php 
						        $no=1;
						        	if (count($anggota)==NULL) {
						        		echo '<td colspan="6">Data Tidak Ada !!!</td>';
						        	}
						        	else {


						        foreach ($anggota as $anggota) { ?>
						        <td><?php echo $no++ ?></td>
						        <td><?php echo $anggota->nis ?></td>
						        <td><?php echo $anggota->nama ?></td>
						        <td><?php echo $anggota->jenis_kelamin ?></td>
						        <td><?php echo $anggota->tanggal_lahir ?></td>
						        <td><?php echo $anggota->tanggal_daftar ?></td>
						        <td><?php echo $anggota->kelas ?></td>
						        <tr>
						        <?php } } ?>
						   </tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</html>
