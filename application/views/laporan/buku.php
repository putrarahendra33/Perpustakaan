<!DOCTYPE html>
<html>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    Data Buku
                    <small>Taken from <a href="http://localhost/ta1/sistem_perpustakaan/main/anggota" target="_blank">localhost</a></small>
                </h2>
            </div>
            <div class="panel panel-default">
  				<div class="panel-heading"><b>Data Buku</b></div>
  					<div class="panel-body">
       						<table class="table table-striped">
						        <thead>
						         <tr>
						         <th>No</th>
						         <th>Kode Buku</th>
						         <th>Judul Buku</th>
						         <th>Pengarang</th>
						         <th>Kategori Buku</th>
						         <th></th>
						         </tr>
						        </thead>
						        <tbody>
						        <?php 
						        $no=1;
						        	if (count($buku)==NULL) {
						        		echo '<td colspan="6">Data Tidak Ada !!!</td>';
						        	}
						        	else {


						        foreach ($buku as $buku) { ?>
						        <td><?php echo $no++ ?></td>
						        <td><?php echo $buku->kode_buku ?></td>
						        <td><?php echo $buku->judul ?></td>
						        <td><?php echo $buku->pengarang ?></td>
						        <td><?php echo $buku->kategori_buku ?></td>
						        <tr>
						        <?php } } ?>
						   </tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</html>
