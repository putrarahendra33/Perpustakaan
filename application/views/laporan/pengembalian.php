<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    $(function(){
        $("#tanggal1").datepicker({
        	format:'yyyy-mm-dd'
    });
                        
        $("#tanggal2").datepicker({
            format:'yyyy-mm-dd'
    });
                        
        $("#tanggal").datepicker({
            format:'yyyy-mm-dd'
    });
 })
</script>
	<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    Transaksi Peminjaman
                    <small>Taken from <a href="http://localhost/ta1/sistem_perpustakaan/anggota" target="_blank">localhost</a></small>
                </h2>
            </div>
            <div class="panel panel-default">
            	<div class="panel-body">
            		<div class="col-md-6">
	                    <div class="row clearfix">
	                        <div class="col-lg-4 form-control-label">
	                            <label for="nama">Tanggal Pinjam</label>
	                        </div>
		                         <div class="col-lg-7">
		                             <div class="form-group">
		                                <div class="form-line">
		                                    <input type="text" id="tanggal1" class="form-control">
		                                </div>
		                            </div>
		                        </div>
	                    	</div>
	                    <div class="row clearfix">
	                        <div class="col-lg-4 form-control-label">
	                            <label for="nama">Tanggal Kembali</label>
	                        </div>
		                         <div class="col-lg-7">
		                             <div class="form-group">
		                                <div class="form-line">
		                                    <input type="text" id="tanggal2" class="form-control" value="">
		                                </div>
		                            </div>
		                        </div>
	                    	</div>
	                   	</div>
	                </div>
	            </div>

<script type="text/javascript">
    $(function(){
        $("#tampilkan").click(function(){
            var tanggal1=$("#tanggal1").val();
            var tanggal2=$("#tanggal2").val();
            
            $.ajax({
                url:"<?php echo site_url('laporan/cari_pinjaman');?>",
                type:"POST",
                data:"tanggal1="+tanggal1+"&tanggal2="+tanggal2,
                cache:false,
                success:function(html){
                    $("#tampil").html(html);
                }
            })
        })
    })
</script>