<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    $(function(){
        
        function loadData(args) {
            //code
            $("#tampil").load("<?php echo site_url('peminjaman/tampil');?>");
        }
        loadData();
        
        function kosong(args) {
            //code
            $("#kode").val('');
            $("#judul").val('');
            $("#pengarang").val('');
        }
        
        $("#nis").click(function(){
            var nis=$("#nis").val();
            
            $.ajax({
                url:"<?php echo site_url('peminjaman/cariAnggota');?>",
                type:"POST",
                data:"nis="+nis,
                cache:false,
                success:function(html){
                    $("#nama").val(html);
                }
            })
        })
        
        $("#kode").keypress(function(){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            
            if(keycode == '13'){
                var kode=$("#kode").val();
            
                $.ajax({
                    url:"<?php echo site_url('peminjaman/cariBuku');?>",
                    type:"POST",
                    data:"kode="+kode,
                    cache:false,
                    success:function(msg){
                        data=msg.split("|");
                        if (data==0) {
                            alert("data tidak ditemukan");
                            $("#judul").val('');
                            $("#pengarang").val('');
                        }else{
                            $("#judul").val(data[0]);
                            $("#pengarang").val(data[1]);
                            $("#tambah").focus();
                        }
                    }
                })
            }
        })
        
        $("#tambah").click(function(){
            var kode=$("#kode").val();
            var judul=$("#judul").val();
            var pengarang=$("#pengarang").val();
            
            if (kode=="") {
                //code
                alert("Kode Buku Masih Kosong");
                return false;
            }else if (judul=="") {
                //code
                alert("Data tidak ditemukan");
                return false
            }else{
                $.ajax({
                    url:"<?php echo site_url('peminjaman/tambah');?>",
                    type:"POST",
                    data:"kode="+kode+"&judul="+judul+"&pengarang="+pengarang,
                    cache:false,
                    success:function(html){
                        loadData();
                        kosong();
                    }
                })    
            }
            
        })
        
        
        $("#simpan").click(function(){
            var nomer=$("#no").val();
            var pinjam=$("#pinjam").val();
            var kembali=$("#kembali").val();
            var nis=$("#nis").val();
            var jumlah=parseInt($("#jumlahTmp").val(),10);
            
            if (nis=="") {
                alert("Pilih Nis Siswa");
                return false;
            }else if (jumlah==0) {
                alert("pilih buku yang akan dipinjam");
                return false;
            }else{
                $.ajax({
                    url:"<?php echo site_url('peminjaman/sukses');?>",
                    type:"POST",
                    data:"nomer="+nomer+"&pinjam="+pinjam+"&kembali="+kembali+"&nis="+nis+"&jumlah="+jumlah,
                    cache:false,
                    success:function(html){
                        alert("Transaksi Peminjaman berhasil");
                        location.reload();
                    }
                })
            }
            
        })
        
        $(".hapus").live("click",function(){
            var kode=$(this).attr("kode");
            
            $.ajax({
                url:"<?php echo site_url('peminjaman/hapus');?>",
                type:"POST",
                data:"kode="+kode,
                cache:false,
                success:function(html){
                    loadData();
                }
            });
        });
        
        $("#cari").click(function(){
            $("#myModal2").modal("show");
        })
        
        $("#caribuku").keyup(function(){
            var caribuku=$("#caribuku").val();
            
            $.ajax({
                url:"<?php echo site_url('peminjaman/pencarianbuku');?>",
                type:"POST",
                data:"caribuku="+caribuku,
                cache:false,
                success:function(html){
                    $("#tampilbuku").html(html);
                }
            })
        })
        
        $(".tambah").live("click",function(){
            var kode=$(this).attr("kode");
            var judul=$(this).attr("judul");
            var pengarang=$(this).attr("pengarang");
            
            $("#kode").val(kode);
            $("#judul").val(judul);
            $("#pengarang").val(pengarang);
            
            $("#myModal2").modal("hide");
        })
        
    })
</script>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    Transaksi Peminjaman
                    <small>Taken from <a href="http://localhost/ta1/sistem_perpustakaan/anggota" target="_blank">localhost</a></small>
                </h2>
            </div>
            <div class="panel panel-default">
            	<div class="panel-body">
            		<form class="form-vertical" action="<?php echo site_url('Peminjaman/simpan');?>" method="post">
            		<div class="col-md-6">
	            		<div class="row clearfix">
	                        <div class="col-lg-4 form-control-label">
	                            <label for="no">No. Transaksi:</label>
	                        </div>
		                         <div class="col-lg-7">
		                             <div class="form-group">
		                                <div class="form-line">
		                                    <input type="text" id="no" name="no" class="form-control" value="<?php echo $noauto; ?>" readonly="readonly">
		                                </div>
		                            </div>
		                        </div>
	                    	</div>
	                    <div class="row clearfix">
	                        <div class="col-lg-4 form-control-label">
	                            <label for="nama">Tanggal Pinjam</label>
	                        </div>
		                         <div class="col-lg-7">
		                             <div class="form-group">
		                                <div class="form-line">
		                                    <input type="text" name="nama" class="form-control" value="<?php echo $tanggalpinjam; ?>" readonly="readonly">
		                                </div>
		                            </div>
		                        </div>
	                    	</div>
	                    <div class="row clearfix">
	                        <div class="col-lg-4 form-control-label">
	                            <label for="nama">Tanggal Kembali</label>
	                        </div>
		                         <div class="col-lg-7">
		                             <div class="form-group">
		                                <div class="form-line">
		                                    <input type="text" name="nama" class="form-control" value="<?php echo $tanggalkembali; ?>" readonly="readonly">
		                                </div>
		                            </div>
		                        </div>
	                    	</div>
	                   	</div>
	                <div class="col-md-6">
	            		<div class="row clearfix">
	                        <div class="col-lg-4 form-control-label">
	                            <label for="nama">Nis</label>
	                        </div>
		                         <div class="col-lg-7">
		                             <div class="form-group">
		                                <select name="nis" id="nis" class="control show-tick">
		                                		<option></option>
                                                <?php foreach($anggota as $anggota) { ?>
                                                <option value="<?php echo $anggota->nis; ?>"><?php echo $anggota->nis; ?></option>
                                                <?php } ?>
                                        </select>
		                            </div>
		                        </div>
	                    	</div>
	                    <div class="row clearfix">
	                        <div class="col-lg-4 form-control-label">
	                            <label for="nama">Nama Siswa</label>
	                        </div>
		                         <div class="col-lg-7">
		                             <div class="form-group">
		                                <div class="form-line">
		                                    <input type="text" id="nama" name="nama" class="form-control">
		                                </div>
		                            </div>
		                        </div>
	                    	</div>
	                   	</div>
	                </div>
	            </div>

	            <div class="panel panel-default">
		            <div class="panel panel-success">
		                <div class="panel-heading">
		                	Data buku
		                </div>

		                <div class="panel-body">
                            <form>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input id="kode" type="text" class="form-control">
                                                <label class="form-label">Kode Buku</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <div class="form-line disabled">
                                                <input id="judul" type="password" class="form-control" disabled>
                                                <label class="form-label">Judul Buku</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <div class="form-line disabled">
                                                <input id="pengarang" type="password" class="form-control" disabled>
                                                <label class="form-label">Pengarang</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    	<button id="tambah" type="button" class="btn btn-primary btn-lg m-l-10 waves-effect"><i class="glyphicon glyphicon-plus"></i></button>
                                        <button id="cari" type="button" class="btn btn-primary btn-lg m-l-10 waves-effect"><i class="glyphicon glyphicon-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    <div id="tampil"></div>

                    <div class="panel-footer">
                    	<button id="simpan" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Simpan</button>
                    </div>
                </div>
        <!-- Modal -->
            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Cari Buku</h4>
                  </div>
                  <div class="modal-body">
                        <div class="form-horizontal">
                            <label class="col-lg-3 control-label">Cari Nama Buku</label>
                            <div class="col-lg-5">
                                <input type="text" name="caribuku" id="caribuku" class="form-control">
                            </div>
                        </div>
                        
                        <div id="tampilbuku"></div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="konfirmasi">Hapus</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

	                    