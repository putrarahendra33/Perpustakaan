<!-- #Top Bar -->
    <section>
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in active" id="settings">
                    <div class="demo-settings">
                        <p>Ubah Nama</p>
                        <?php foreach($user as $user) { ?>
                        <ul class="setting-list">
                            <li>
                                <span><?php echo $user->nama_asli ?></span>
                                <div class="body">
                                    <!-- Include Nama!! -->
                                </div>
                            </li>
                            <li>
                                <span>Nama Baru</span>
                                <div class="body">
                                    
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="nama_user" class="form-control" placeholder="Masukan Nama Baru" value="<?php echo $user->nama_asli ?>">
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">Ubah!</button>
                                </div>
                            </li>
                        </ul>
                        <p>Ubah Password</p>
                        <ul class="setting-list">
                            </li>
                            <li>
                                <div class="body">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="password" id="password" class="form-control" placeholder="Masukan Password Baru" value="<?php echo $user->password ?>">
                                            </div>
                                        </div>
                                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">Ubah!</button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php } ?>
        </aside>
        <!-- End Header -->
    </section>